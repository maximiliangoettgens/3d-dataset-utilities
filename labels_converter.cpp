#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

using namespace std;
namespace fs = boost::filesystem;
namespace po = boost::program_options;

int main(int argc, char **argv) {

    //Some variables with wide scope
    po::variables_map vm;
    fs::path input_dir;
    fs::path output_dir;

    po::options_description desc("Allowed options");
    desc.add_options()
            ("help", "Print help message")
            ("input_directory", po::value<string>(), "Directory containing Pointclouds to convert.")
            ("output_directory", po::value<string>(), "Directory to store converted Pointclouds.");

    // Parse arguments
    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            cout << "This tool converts KITTI labels from field 'intensity' (matlab workaround) to 'label'. \n"
                 << "To use it, specify an 'input_directory' containing the pointclouds to convert and an 'output_directory' for the converted clouds. \n"
                 << "Note that at the moment only .pcd and .ply files are supported.\n";
            return 0;
        }
        //check if input is specified
        if (!vm.count("input_directory")) {
            cerr << "Please provide an input-directory." << endl;
            return -1;
        }
        if (!vm.count("output_directory")) {
            cerr << "Please provide an output-directory." << endl;
            return -1;
        }
        //check if input is specified correctly
        input_dir = vm["input_directory"].as<string>();
        if (!fs::is_directory(input_dir)) {
            cerr << "Unable to find specified input-directory: " + input_dir.string() + " - Is it a file?" << endl;
            return -1;
        }
        output_dir = vm["output_directory"].as<string>();
        if (output_dir.string().substr((output_dir.string().size() - 1)) != "/")
            output_dir = output_dir.string() + "/";
        if (!fs::is_directory(output_dir)) {
            cout << "Unable to find specified input-directory: " + output_dir.string() + " - Trying to create one." << endl;
            fs::create_directory(output_dir);
        }
        if (is_empty(input_dir)) {
            cerr << "The specified input directory is empty!" << endl;
            return -1;
        }
    } catch (po::error &e) {
        cerr << "ERROR PARSING ARGUMENTS: " << e.what() << endl;
        return -1;
    }
    cout << "Beginning to convert labels from KITTI to generic..." << endl;

    try {
        typedef vector<fs::path> vec;
        // store paths, so we can sort them later
        vec v;

        copy(fs::directory_iterator(input_dir), fs::directory_iterator(), back_inserter(v));

        // sort, since directory iteration is not ordered on some file systems
        sort(v.begin(), v.end());

        //iterate over all files in input_dir directory
        vec::const_iterator it(v.begin());

        for(vec::const_iterator it = v.begin(); it != v.end(); ++it) {

            string filepath = it.base()->string();
            string filename;

            size_t filename_pos = it->string().find_last_of('/');
            if (filename_pos != std::string::npos) {
                filename.assign(it->string().begin() + filename_pos + 1,
                                it->string().end());
            }

            cout << "Processing: " + filename << endl;

            pcl::PointCloud<pcl::PointXYZI>::Ptr input_cloud(new pcl::PointCloud<pcl::PointXYZI>());
            pcl::PointCloud<pcl::PointXYZRGBL>::Ptr output_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>());

            //Load Pointcloud
            if (filepath.substr(filepath.size() -3) == "pcd")
                pcl::io::loadPCDFile<pcl::PointXYZI>(filepath, *input_cloud);
            else if (filepath.substr(filepath.size() -3) == "ply")
                pcl::io::loadPLYFile<pcl::PointXYZI>(filepath, *input_cloud);
            else{
                cerr << filepath << " is not a valid pointcloud - continuing..." << endl;
                continue;
            }
            for (pcl::PointCloud<pcl::PointXYZI>::iterator points_it = input_cloud->begin (); points_it != input_cloud->end (); ++points_it) {
                //Convert labels
                //Void, Sky, 2x'Reserved', Pedestrian, Bicycle -> Clutter
                pcl::PointXYZRGBL point;
                if (points_it->intensity == 0 ||
                    points_it->intensity == 2 ||
                    points_it->intensity == 7 ||
                    points_it->intensity == 8){
                    point.label = 0;
                }
                //Road, Sidewalk, Lanemarking -> Groundplane
                if (points_it->intensity == 3 ||
                    points_it->intensity == 5 ){
                    point.label = 1;
                }
                //Vegetation -> Vegetation
                if (points_it->intensity == 4 ){
                    point.label = 2;
                }
                //Building -> Building
                if (points_it->intensity == 1 ){
                    point.label = 3;
                }
                //Pole, Traffic Sign, Traffic Light, Fence -> Man-made structure
                if (points_it->intensity == 10 ||
                    points_it->intensity == 9 ){
                    point.label = 4;
                }
                //Car -> Car
                if (points_it->intensity == 6){
                    point.label = 5;
                }
                point.x = points_it->x;
                point.y = points_it->y;
                point.z = points_it->z;
                point.r = 255;
                point.g = 255;
                point.b = 255;
                output_cloud->push_back(point);
            }
            //Write output pointcloud
            pcl::io::savePLYFileASCII(output_dir.string() + filename, *output_cloud);
        }
    }
    catch (const fs::filesystem_error &e) {
        cout << e.what() << '\n';
    }

}
