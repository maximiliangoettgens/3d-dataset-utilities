#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

using namespace std;
namespace fs = boost::filesystem;
namespace po = boost::program_options;

int main(int argc, char **argv) {

    //Some variables with wide scope
    po::variables_map vm;
    fs::path input_dir;
    fs::path output_dir;

    po::options_description desc("Allowed options");
    desc.add_options()
            ("help", "Print help message")
            ("input_directory", po::value<string>(), "Directory containing Pointclouds to convert.")
            ("output_directory", po::value<string>(), "Directory to store converted Pointclouds.");

    // Parse arguments
    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            cout << "This tool converts SYNTHIA labels to generic labels. To use it, specify an 'input_directory' \n"
                 << "containing the pointclouds to display and an 'output_directory' for the converted clouds. \n"
                 << "Note that at the moment only .pcd and .ply files are supported.\n";
            return 0;
        }
        //check if input is specified
        if (!vm.count("input_directory")) {
            cerr << "Please provide an input-directory." << endl;
            return -1;
        }
        if (!vm.count("output_directory")) {
            cerr << "Please provide an output-directory." << endl;
            return -1;
        }
        //check if input is specified correctly
        input_dir = vm["input_directory"].as<string>();
        if (!fs::is_directory(input_dir)) {
            cerr << "Unable to find specified input-directory: " + input_dir.string() + " - Is it a file?" << endl;
            return -1;
        }
        output_dir = vm["output_directory"].as<string>();
        if (output_dir.string().substr((output_dir.string().size() - 1)) != "/")
            output_dir = output_dir.string() + "/";
        if (!fs::is_directory(output_dir)) {
            cout << "Unable to find specified input-directory: " + output_dir.string() + " - Trying to create one." << endl;
            fs::create_directory(output_dir);
        }
        if (is_empty(input_dir)) {
            cerr << "The specified input directory is empty!" << endl;
            return -1;
        }
    } catch (po::error &e) {
        cerr << "ERROR PARSING ARGUMENTS: " << e.what() << endl;
        return -1;
    }

    cout << "Beginning to convert labels from SYNTHIA to generic..." << endl;

    try {
        typedef vector<fs::path> vec;
        // store paths, so we can sort them later
        vec v;

        copy(fs::directory_iterator(input_dir), fs::directory_iterator(), back_inserter(v));

        // sort, since directory iteration is not ordered on some file systems
        sort(v.begin(), v.end());

        //iterate over all files in input_dir directory
        vec::const_iterator it(v.begin());

        for(vec::const_iterator it = v.begin(); it != v.end(); ++it) {

            string filepath = it.base()->string();
            string filename;

            size_t filename_pos = it->string().find_last_of('/');
            if (filename_pos != std::string::npos) {
                filename.assign(it->string().begin() + filename_pos + 1,
                                it->string().end());
            }

            cout << "Processing: " + filename << endl;

            pcl::PointCloud<pcl::PointXYZRGBL>::Ptr point_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>());

            //Load Pointcloud
            if (filepath.substr(filepath.size() -3) == "pcd")
                pcl::io::loadPCDFile<pcl::PointXYZRGBL>(filepath, *point_cloud);
            else if (filepath.substr(filepath.size() -3) == "ply")
                pcl::io::loadPLYFile<pcl::PointXYZRGBL>(filepath, *point_cloud);
            else{
                cerr << filepath << " is not a valid pointcloud - continuing..." << endl;
                continue;
            }

            for (pcl::PointCloud<pcl::PointXYZRGBL>::iterator points_it = point_cloud->begin (); points_it != point_cloud->end (); ++points_it) {
                //Convert labels
                //Void, Sky, 2x'Reserved', Pedestrian, Bicycle -> Clutter
                if (//points_it->label == 0 ||
                    points_it->label == 1 ||
                    points_it->label == 10 ||
                    points_it->label == 11 ||
                    points_it->label == 13 ||
                    points_it->label == 14){
                    points_it->label = 0;
                    continue;
                }
                //Road, Sidewalk, Lanemarking -> Groundplane
                if (points_it->label == 3 ||
                    points_it->label == 4 ||
                    points_it->label == 12){
                    points_it->label = 1;
                    continue;
                }
                //Vegetation -> Vegetation
                if (points_it->label == 6 ){
                    points_it->label = 2;
                    continue;
                }
                //Building -> Building
                if (points_it->label == 2 ){
                    points_it->label = 3;
                    continue;
                }
                //Most of Void, Pole, Traffic Sign, Traffic Light, Fence -> Man-made structure
                if (points_it->label == 0 ||
                    points_it->label == 5 ||
                    points_it->label == 7 ||
                    points_it->label == 9 ||
                    points_it->label == 15){
                    points_it->label = 4;
                    continue;
                }
                //Car -> Car
                if (points_it->label == 8){
                    points_it->label = 5;
                    continue;
                }
                points_it->label = 0;   //default - should never be called though
            }

            //Write output pointcloud
            pcl::io::savePLYFileASCII(output_dir.string() + filename, *point_cloud);
        }
    }
    catch (const fs::filesystem_error &e) {
        cout << e.what() << '\n';
    }

}
