#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/common/transforms.h>

using namespace std;
namespace fs = boost::filesystem;
namespace po = boost::program_options;

int main(int argc, char **argv) {

    //Some variables with wide scope
    po::variables_map vm;
    fs::path basedir;
    string converted_dir;

    po::options_description desc("Allowed options");
    desc.add_options()
            ("help", "Print help message")
            ("input_directory", po::value<string>(), "Directory where raw KITTI .bin files are stored.")
            ("output_format", po::value<string>(), "Options: \n binpcd [default] \n binply \n asciipcd \n asciiply");

    // Parse arguments
    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            cout << "This tool transforms data provided in KITTI binary format into well known .ply or .pcd format. \n"
                 << "To use this tool you need to provide an 'input directory' where all .bin files \n"
                 << "for conversion are stored. Optionally, you can specify the 'output_format' (default is binary-pcd)";
            return 0;
        }
        //check if input is specified
        if (!vm.count("input_directory")) {
            cerr << "Please provide an input-directory." << endl;
            return -1;
        }

        // Load data
        basedir = vm["input_directory"].as<string>();
        if (!is_directory(basedir)) {
            cerr << "Unable to find specified input-directory: " + basedir.string() + " - Is it a file?" << endl;
            return -1;
        }
        if (basedir.string().substr((basedir.string().size() - 1)) == "/")
        converted_dir = basedir.string().substr(0, basedir.string().size() - 1) + "_converted/";
        else
            converted_dir = basedir.string() + "_converted/";
        boost::filesystem::create_directory(fs::path(converted_dir));
    } catch (po::error &e) {
        cerr << "ERROR PARSING ARGUMENTS: " << e.what() << endl;
        return -1;
    }

    try {
        cout << "Starting conversion!" << endl;
        cout << "Depending on the amount of data, this might take a long time." << endl;
        cout << "You might want to go watch a ball game while this is running..." << endl;

        // store paths, so we can sort them later
        vector<fs::path> files_vector;
        std::string filename;
        copy(fs::directory_iterator(basedir), fs::directory_iterator(), back_inserter(files_vector));

        sort(files_vector.begin(), files_vector.end());             // sort, since directory iteration is not ordered in some file systems

        //iterate over all files in basedir directory
        for (vector<fs::path>::const_iterator it(files_vector.begin()), it_end(files_vector.end()); it != it_end; ++it) {

            const fs::path *full_directory = it.base();

            size_t filename_pos = full_directory->string().find_last_of('/');
            if (filename_pos != std::string::npos) {
                filename.assign(full_directory->string().begin() + filename_pos + 1,
                                full_directory->string().end());
            }

            cout << "Processing: " << filename << '\n';

            // allocate 4 MB buffer (only ~130*4*4 KB are needed)
            int32_t num = 1000000;
            float *data = (float *) malloc(num * sizeof(float));

            // pointers
            float *px = data + 0;
            float *py = data + 1;
            float *pz = data + 2;
            float *pr = data + 3;

            // load point cloud in KITTI bin format
            FILE *stream;
            stream = fopen(string(basedir.string() + "/" + filename).c_str(), "rb");
            num = fread(data, sizeof(float), num, stream) / 4;

            //generate Pointcloud object and header
            pcl::PointCloud<pcl::PointXYZ> point_cloud;
            point_cloud.width = num;
            point_cloud.height = 1;
            point_cloud.is_dense = false;
            point_cloud.points.resize(point_cloud.width * point_cloud.height);

            //copy points into pointcloud object
            for (int32_t i = 0; i < num; i++) {
                point_cloud.points[i].x = *px;
                point_cloud.points[i].y = *py;
                point_cloud.points[i].z = *pz;

                px += 4;
                py += 4;
                pz += 4;
                pr += 4;
            }

            Eigen::Matrix4f correction; //T_(cam, global) / velodyne is mounted ~1.7m above ground
            //TODO: Make a ground-plane detection for exact transforms
            correction <<
                        1.0f,  0.0f,  0.0f, 0.0f,
                        0.0f,  1.0f,  0.0f, 0.0f,
                        0.0f,  0.0f,  1.0f, 1.7f,
                        0.0f,  0.0f,  0.0f, 1.0f;

            pcl::transformPointCloud(point_cloud , point_cloud , correction);

            //save pointcloud in specified format
            if (vm.count("output_format")) {
                string query = vm["output_format"].as<string>();

                filename = filename.substr(0, filename.size() - 4);

                if (query == "binpcd") {
                    pcl::io::savePCDFileBinary(converted_dir + filename + ".pcd", point_cloud);
                } else if (query == "asciipcd") {
                    pcl::io::savePCDFileASCII(converted_dir + filename + ".pcd", point_cloud);
                } else if (query == "binply") {
                    pcl::io::savePLYFileBinary(converted_dir + filename + ".ply", point_cloud);
                } else if (query == "asciiply") {
                    pcl::io::savePLYFileASCII(converted_dir + filename + ".ply", point_cloud);
                } else { //default
                    pcl::io::savePCDFileBinary(converted_dir + filename + ".pcd", point_cloud);
                }
            } else { //default
                pcl::io::savePCDFileBinary(converted_dir + filename + ".pcd", point_cloud);
            }
            fclose(stream);
            free(data);
        }

        std::cout << "Done!" << std::endl;
        return 0;
    }
    catch (const fs::filesystem_error &ex) {
        cout << ex.what() << '\n';
    }

}