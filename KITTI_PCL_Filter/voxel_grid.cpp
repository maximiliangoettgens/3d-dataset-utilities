#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/statistical_outlier_removal.h>

int main(int argc, char **argv) {

    pcl::PointCloud<pcl::PointXYZ>::Ptr downsampled (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr final_cloud (new pcl::PointCloud<pcl::PointXYZ>);


    //check if input is specified correctly
    if (argc < 2) {
        cout << "To few input arguments have been specified. Please provide an input-file." << endl;
        return 0;
    }

    // Replace the path below with the path where you saved your file
    pcl::io::loadPCDFile<pcl::PointXYZ>(std::string(argv[1]), *cloud);

    std::cout << "PointCloud before filtering: " << cloud->width * cloud->height
              << " data points (" << pcl::getFieldsList(*cloud) << ").";


    pcl::VoxelGrid<pcl::PointXYZ> voxel_grid;
    voxel_grid.setInputCloud (cloud);
    voxel_grid.setLeafSize(0.04f, 0.04f, 0.04f);

    clock_t tStart = clock();

    voxel_grid.filter(*downsampled);

    printf("Time taken: %.2fs\n", (double) (clock() - tStart) / CLOCKS_PER_SEC);
    tStart = clock();
    std::cout << "Cloud after filtering: " << downsampled->size() << std::endl;

    pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
    sor.setInputCloud (downsampled);
    sor.setMeanK (50);
    sor.setStddevMulThresh (1.0);
    sor.filter (*final_cloud);

    printf("Time taken: %.2fs\n", (double) (clock() - tStart) / CLOCKS_PER_SEC);
    std::cout << "Cloud after outlier removal: " << final_cloud->size() << std::endl;

    pcl::visualization::PCLVisualizer viewer("xyz");
    viewer.setBackgroundColor(0, 0, 0);
    viewer.addPointCloud<pcl::PointXYZ>(downsampled, "cloud");
    viewer.spin();

}
