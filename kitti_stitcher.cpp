#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl/registration/icp_nl.h>
#include <math.h>

using namespace std;
using namespace boost::filesystem;
using namespace boost::program_options;

typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef pcl::PointNormal PointNormalT;
typedef pcl::PointCloud<PointNormalT> PointCloudWithNormals;

//structure to handle set of poinclaas
struct PCD {
    PointCloud::Ptr cloud;
    string f_name;
    Eigen::Matrix4f transformation;

    PCD() : cloud(new PointCloud) {};
};

//Struct to handle program arguments
struct ProgramArgs {
    path basedir;
    string groundtruthFile;
    string outputFormat;
    int batchsize;
    float voxelgridSize;

    ProgramArgs() : groundtruthFile(""), outputFormat("asciiply"), batchsize(50), voxelgridSize(0.1f) {};
};

//Standard transformations
static Eigen::Matrix4f globalToCamera = (Eigen::Matrix4f() << 0.0f, -1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, -1.0f, 0.0f,
        1.0, 0.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f).finished();

static Eigen::Matrix4f cameraToGlobal = (Eigen::Matrix4f() << 0.0f, 0.0f, 1.0f, 0.0f,
        -1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, -1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f).finished();


bool parseArguments(int argc, char **argv, ProgramArgs &args) {

    variables_map vm;

    options_description desc("Allowed options");
    desc.add_options()
            ("help", "Print help message")
            ("input_directory", value<string>(), "Directory containing Pointclouds to stitch.")
            ("ground_truth", value<string>(), "File containing Ground Truth Transformations. [optional]")
            ("batch_size", value<int>(),
             "No. of pointclouds to stitch at once. (choose low if memory is an issue) [default=50]")
            ("voxelgrid_size", value<float>(), "Size of the voxelgrid filter for data reduction (in meters). "
                                               "[default=0.1 ; set 0 for no filter]")
            ("output_format", value<string>(), "Options: \n binpcd \n binply \n asciipcd \n asciiply [default]");;

    // Parse arguments
    try {
        store(parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            cout << "This is a pointcloud registration utility. To use it, specify an 'input_directory'\n"
                 << "containing the pointclouds to register. If available, a set of pre-registration-transformations\n"
                 << "can be loaded as initial guesses for better registration performance (ground_truth). 'Ground truth'\n"
                 << "transformations have to be provided as one .txt file containing a list of 4x4 TF matricies \n"
                 << "(16 elements each) all in one row. Providing a GT file is optional. Please note that if no GT\n"
                 << "file is provided, the raw pointcloud input must be roughly pre-aligned in order for registration\n"
                 << "to produce meaningful results.\n";
            return false;
        }

        //check if input directory is specified correctly
        if (!vm.count("input_directory")) {
            cerr << "Please provide an input-directory." << endl;
            return false;
        }
        path basedir = vm["input_directory"].as<string>();
        if (!is_directory(basedir)) {
            cerr << "Unable to find specified input-directory: " + basedir.string() + " - Is it a file?" << endl;
            return false;
        }
        if (is_empty(basedir)) {
            cerr << "The specified input directory is empty!" << endl;
            return false;
        } else
            args.basedir = basedir;

        //check if GT-file is specified correctly
        if (vm.count("ground_truth")) {
            string groundtruthFile = vm["ground_truth"].as<string>();
            if (path(groundtruthFile).extension().compare(".txt")) {
                cerr << "Invalid ground truth file: " + groundtruthFile << endl;
                return false;
            } else
                args.groundtruthFile = groundtruthFile;
        }

        //check batch size
        if (vm.count("batch_size"))
            args.batchsize = vm["batch_size"].as<int>();

        //Check voxelgrid size
        if (vm.count("voxelgrid_size"))
            args.voxelgridSize = vm["voxelgrid_size"].as<float>();

        //Check Output format
        if (vm.count("output_format")) {
            args.outputFormat = vm["output_format"].as<string>();
        }

    } catch (boost::program_options::error &e) {
        cerr << "ERROR PARSING ARGUMENTS: " << e.what() << endl;
        return false;
    }
    return true;
}

bool loadFileListAndGroundTruth(ProgramArgs &args, vector<path> &filesList,
                                bool hasGroundTruth, vector<Eigen::Matrix4f> &GT) {

    copy(directory_iterator(args.basedir), directory_iterator(), back_inserter(filesList));

    // sort, since directory iteration is not ordered on some file systems
    sort(filesList.begin(), filesList.end());

    //Ground Truth Transformations

    if (hasGroundTruth) {
        //Load the actual ground truth from file
        std::fstream gt_stream(args.groundtruthFile.c_str(), std::ios_base::in);
        float a, b, c, d, e, f, g, h, i, j, k, l;
        Eigen::Matrix4f tempGT;

        for (vector<path>::const_iterator it(filesList.begin()), it_end(filesList.end()); it != it_end; it++) {

            //write groundtruth transformation (via float: for some reason this does not work directly)
            gt_stream >> a >> b >> c >> d >> e >> f >> g >> h >> i >> j >> k >> l;

            tempGT <<
                   a, b, c, d,
                    e, f, g, h,
                    i, j, k, l,
                    0.0f, 0.0f, 0.0f, 1.0f;
            //Odometry data comes in camera coordinate frame - this is the transformation: T_(global,cam)
            GT.push_back(tempGT * globalToCamera);
        }
    } else{
        //This implies the 'initial guess' is identity
        //It is actually unnecessary to transform into camera frame,
        //however, it is easier and more consistent with the rest of the code
        //to do all actual code in camera frame
        GT.push_back(globalToCamera);
    }
    return true;
}

bool loadBatch(bool hasGroundTruth, vector<path> &filesList, int startIndex, int batchsize, vector<Eigen::Matrix4f> &GT,
               vector<PCD, Eigen::aligned_allocator<PCD> > &batch) {

    try {
        Eigen::Matrix4f identity;
        identity.setIdentity();
        for (int index = startIndex; index < startIndex + batchsize; index++) {

            const path full_directory = filesList[index];
            string filename;

            size_t filename_pos = full_directory.string().find_last_of('/');
            if (filename_pos != string::npos) {
                filename.assign(full_directory.string().begin() + filename_pos + 1,
                                full_directory.string().end());
            }

            cout << "Loading: " << filename << '\n';

            // Load the cloud and save it into the global list of data
            PCD m;
            m.f_name = filename;


            if (pcl::io::loadPCDFile(full_directory.string(), *m.cloud) == -1) {
                //PCD load failed
                cout << "Failed to read PCD, trying PLY..." << endl;
                if (pcl::io::loadPLYFile(full_directory.string(), *m.cloud) == -1) {
                    //Also PLY load failed
                    string message = "Couldn't read file: " + full_directory.string() + "\n";
                    PCL_ERROR (message.c_str());
                    return false;
                }
            }

            //Apply affine transformation according to groundtruth
            if (hasGroundTruth) {
                pcl::transformPointCloud(*m.cloud, *m.cloud, (Eigen::Matrix4f) GT[index]);
            } else
                //GT transformation (guess) is always the same anyway
                pcl::transformPointCloud(*m.cloud, *m.cloud, (Eigen::Matrix4f) GT[0]);

            //Set identity as initial transformation
            m.transformation = identity;
            batch.push_back(m);
        }
        cout << "Loaded " << batch.size() << " pointclouds." << endl;
        return true;
    }
    catch (const filesystem_error &ex) {
        cout << ex.what() << '\n';
        return false;
    }
}

bool circularFilter(const PointCloud::Ptr cloud_src, PointCloud::Ptr &cloud_output) {
    if (cloud_output->points.size() > 0) {
        cerr
                << "Unable to apply circular filter: A non-empty target container was passed - copying input cloud with no action"
                << endl;
        cloud_output->points = cloud_src->points;
        return false;
    }
    float threashold = 5.0f;
    for (size_t i = 0; i < cloud_src->points.size(); i++) {
        if (sqrt(pow(cloud_src->points[i].x, 2) + pow(cloud_src->points[i].x, 2)) < threashold) {
            cloud_output->push_back(cloud_src->points[i]);
        }
    }
    return true;
}

void pairAlign(const PointCloud::Ptr cloud_src, const PointCloud::Ptr cloud_tgt,
               Eigen::Matrix4f &targetToSource) {
//TODO: ###############################################################################################
//TODO: Fine-tune Registration parameters
//TODO: ###############################################################################################

    //filter points for faster registration
    PointCloud::Ptr temp_src(new PointCloud);
    PointCloud::Ptr temp_tgt(new PointCloud);

    //Apply circular filter to disregard far away points
    //since these have large variances even when registered correctly
    circularFilter(cloud_src, temp_src);
    circularFilter(cloud_tgt, temp_tgt);

    PointCloud::Ptr src(new PointCloud);
    PointCloud::Ptr tgt(new PointCloud);

    //Voxelgridfilter for faster processing
    pcl::VoxelGrid<PointT> grid;
    grid.setLeafSize(0.1, 0.1, 0.1);

    grid.setInputCloud(temp_src);
    grid.filter(*src);
    grid.setInputCloud(temp_tgt);
    grid.filter(*tgt);

    //calculate normals for registration
    PointCloudWithNormals::Ptr points_with_normals_src(new PointCloudWithNormals);
    PointCloudWithNormals::Ptr points_with_normals_tgt(new PointCloudWithNormals);

    pcl::NormalEstimation<PointT, PointNormalT> norm_est;
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>());
    norm_est.setSearchMethod(tree);
    norm_est.setKSearch(30);

    norm_est.setInputCloud(src);
    norm_est.compute(*points_with_normals_src);
    pcl::copyPointCloud(*src, *points_with_normals_src);

    norm_est.setInputCloud(tgt);
    norm_est.compute(*points_with_normals_tgt);
    pcl::copyPointCloud(*cloud_tgt, *points_with_normals_tgt);

    // Initialize registration-algorithm
    pcl::IterativeClosestPointWithNormals<PointNormalT, PointNormalT> reg;
    reg.setMaximumIterations(25);
    reg.setMaxCorrespondenceDistance(0.4);
    reg.setInputSource(points_with_normals_src);
    reg.setInputTarget(points_with_normals_tgt);

    //run alignment
    reg.align(*points_with_normals_src);

    //get Transformation
    targetToSource = reg.getFinalTransformation();
}

//TODO: ###############################################################################################
//TODO: Implement all this without the ground truth transformation in the beginning
//TODO: Actually make an implementation that gives you odometry estimation based on
//TODO: registration transformations
//TODO: PCD.filenmae is sort of unnecessary
//TODO: ###############################################################################################

int main(int argc, char **argv) {

    //Parse Input Arguments
    ProgramArgs args;
    if (!parseArguments(argc, argv, args)) return (-1);
    bool hasGroundTruth = !(args.groundtruthFile == "");

    //Generate Files List and load Ground Truth Transformations
    vector<path> filesList;
    vector<Eigen::Matrix4f> GT;
    if (!loadFileListAndGroundTruth(args, filesList, hasGroundTruth, GT)) return (-1);

    //Inform user about program start
    cout << "Starting stitching!" << endl;
    cout << "Depending on the amount of data, this might take a (very) long time." << endl;

    //prepare target directory for outputs
    string stitched_dir;
    if (args.basedir.string().substr((args.basedir.string().size() - 1)) == "/")
        stitched_dir = args.basedir.string().substr(0, args.basedir.string().size() - 1) + "_stitched/";
    else
        stitched_dir = args.basedir.string() + "_stitched/";
    boost::filesystem::create_directory(path(stitched_dir));

    //perform registration in batches
    for (int batchIndex = 0; batchIndex < (filesList.size() / args.batchsize); batchIndex++) {
        //Load Batch
        std::vector<PCD, Eigen::aligned_allocator<PCD> > batch;
        loadBatch(hasGroundTruth, filesList, batchIndex * args.batchsize, args.batchsize, GT, batch);

        //Align pointclouds in batch pairwise
        for (size_t i = 1; i < batch.size(); i++) {

            PCL_INFO ("Aligning %s with %s.\n", batch[i - 1].f_name.c_str(), batch[i].f_name.c_str());

            Eigen::Matrix4f targetToSource;
            //initialize TF with identity
            targetToSource
                    << 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f;
            //pairAlign(batch[i - 1].cloud, batch[i].cloud, targetToSource);

            //keep past transformations up to date for storing
            for (int j = 0; j < i; j++)
                batch[j].transformation = targetToSource * batch[j].transformation;
        }

        //Apply the calculated transformations
        for (size_t i = 1; i < batch.size(); i++)
            pcl::transformPointCloud(*batch[i].cloud, *batch[i].cloud, batch[i].transformation);

        //generate global pointcloud for output
        pcl::PointCloud<pcl::PointXYZ>::Ptr global_cloud(new PointCloud);

        for (int i = 0; i < batch.size(); i++) {
            *global_cloud += *batch[i].cloud;
        }

        //Center coordinate system
        pcl::transformPointCloud(*global_cloud, *global_cloud, (Eigen::Matrix4f)(GT[batchIndex*args.batchsize]*globalToCamera.inverse()).inverse() );

        //Transform back from camera frame into global frame (Semantic-Segmentation works in global cosy)
        pcl::transformPointCloud(*global_cloud, *global_cloud, cameraToGlobal);

        //Apply filter for artefact removal below groundplane
        pcl::PassThrough<pcl::PointXYZ> pass;
        pass.setFilterFieldName("z");
        pass.setFilterLimits(-0.5f, 10000.0f);

        pass.setInputCloud(global_cloud);
        pass.filter(*global_cloud);

        //Apply filter for data reduction
        if (!args.voxelgridSize == 0.0f) {
            pcl::VoxelGrid<pcl::PointXYZ> voxel_grid;
            voxel_grid.setInputCloud(global_cloud);
            voxel_grid.setLeafSize(args.voxelgridSize, args.voxelgridSize, args.voxelgridSize);

            voxel_grid.filter(*global_cloud);
        }
        //Proper filenames for later iterating
        char beginIdx[6];
        char endIdx[6];

        sprintf(beginIdx, "%06d", batchIndex * args.batchsize);
        sprintf(endIdx, "%06d", (batchIndex + 1) * args.batchsize);

        //save pointcloud in specified format

        string filename = "Scene_global_" + string(beginIdx) + "_" + string(endIdx);

        if (args.outputFormat == "binpcd")
            pcl::io::savePCDFileBinary(stitched_dir + filename + ".pcd", *global_cloud);
        else if (args.outputFormat == "asciipcd")
            pcl::io::savePCDFileASCII(stitched_dir + filename + ".pcd", *global_cloud);
        else if (args.outputFormat == "binply")
            pcl::io::savePLYFileBinary(stitched_dir + filename + ".ply", *global_cloud);
        else if (args.outputFormat == "asciiply")
            pcl::io::savePLYFileASCII(stitched_dir + filename + ".ply", *global_cloud);
        else  //default in case of wrong input
            pcl::io::savePLYFileASCII(stitched_dir + filename + ".ply", *global_cloud);

    }
}