#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#define RELOAD_STATE_NEXT 1
#define RELOAD_STATE_PREVIOUS -1
#define RELOAD_STATE_EXIT 2

using namespace std;
namespace fs = boost::filesystem;
namespace po = boost::program_options;

int reload = RELOAD_STATE_NEXT;
string labelmode = "";
bool keyPressed = false;
bool first_loop = true;
bool display_labels = false;

void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event,
                           void *viewer_void) {
    //prevent view spinner from running this callback multiple times
    if (!event.keyDown()) return;

    switch (event.getKeySym().c_str()[0]) {
        case 'd': {
            reload = RELOAD_STATE_NEXT;
            break;
        }
        case 'a': {
            reload = RELOAD_STATE_PREVIOUS;
            break;
        }
        case 's': {
            reload = RELOAD_STATE_EXIT;
            break;
        }
        default:
            return;
    }
    keyPressed = true;
}

void convertColorsByLabels(pcl::PointCloud<pcl::PointXYZRGBL>::Ptr &point_cloud, string &labelmode){
    if (labelmode == "")
        return;
    if (labelmode == "semantic3D") {
        for (pcl::PointCloud<pcl::PointXYZRGBL>::iterator points_it = point_cloud->begin();
             points_it != point_cloud->end(); ++points_it) {
            if (points_it->label == 1) {
                points_it->r = 200;
                points_it->g = 200;
                points_it->b = 200;
            }
            if (points_it->label == 2) {
                points_it->r = 0;
                points_it->g = 70;
                points_it->b = 0;
            }
            if (points_it->label == 3) {
                points_it->r = 0;
                points_it->g = 255;
                points_it->b = 0;
            }
            if (points_it->label == 4) {
                points_it->r = 255;
                points_it->g = 255;
                points_it->b = 0;
            }
            if (points_it->label == 5) {
                points_it->r = 255;
                points_it->g = 0;
                points_it->b = 0;
            }
            if (points_it->label == 6) {
                points_it->r = 148;
                points_it->g = 0;
                points_it->b = 211;
            }
            if (points_it->label == 7) {
                points_it->r = 0;
                points_it->g = 255;
                points_it->b = 255;
            }
            if (points_it->label == 8) {
                points_it->r = 255;
                points_it->g = 8;
                points_it->b = 127;
            }
        }
    }
    if (labelmode == "generic") {
        for (pcl::PointCloud<pcl::PointXYZRGBL>::iterator points_it = point_cloud->begin();
             points_it != point_cloud->end(); ++points_it) {
            if (points_it->label == 0) {
                points_it->r = 255;
                points_it->g = 255;
                points_it->b = 255;
            }
            if (points_it->label == 1) {
                points_it->r = 125;
                points_it->g = 125;
                points_it->b = 125;
            }
            if (points_it->label == 2) {
                points_it->r = 0;
                points_it->g = 255;
                points_it->b = 0;
            }
            if (points_it->label == 3) {
                points_it->r = 255;
                points_it->g = 0;
                points_it->b = 0;
            }
            if (points_it->label == 4) {
                points_it->r = 148;
                points_it->g = 0;
                points_it->b = 211;
            }
            if (points_it->label == 5) {
                points_it->r = 0;
                points_it->g = 0;
                points_it->b = 255;
            }
        }
    }
  if (labelmode == "bgr") {
    for (pcl::PointCloud<pcl::PointXYZRGBL>::iterator points_it = point_cloud->begin();
         points_it != point_cloud->end(); ++points_it) {
      int r = points_it->r;
      int b = points_it->b;
      points_it->r = b;
      points_it->b = r;
    }
  }
}

int main(int argc, char **argv) {

    //Some variables with wide scope
    po::variables_map vm;
    fs::path basedir;

    po::options_description desc("Allowed options");
    desc.add_options()
            ("help", "Print help message")
            ("input_directory", po::value<string>(), "Directory containing Pointclouds to display.")
            ("label_mode", po::value<string>(), "Make Pointcloud colors conform label scheme of specific dataset. \n"
                                                "Options are: 'semantic3D', 'generic', 'SYNTHIA'");

    // Parse arguments
    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            cout << "This is a lightweight pointcloud display utility. To use it, specify an 'input_directory' \n"
                 << "containing the pointclouds to display. Label colors can be displayed with the 'display_labels' option."
                 << "Note that at the moment only .pcd and .ply files are supported. (XYZ and XYZRGB)\n";
            return 0;
        }
        //check if input is specified
        if (!vm.count("input_directory")) {
            cerr << "Please provide an input-directory." << endl;
            return -1;
        }
        //check if input is specified correctly
        basedir = vm["input_directory"].as<string>();
        if (!fs::is_directory(basedir)) {
            cerr << "Unable to find specified input-directory: " + basedir.string() + " - Is it a file?" << endl;
            return -1;
        }if (is_empty(basedir)) {
            cerr << "The specified input directory is empty!" << endl;
            return -1;
        }
        //check if color mode for labels is specified
        if (vm.count("label_mode")){
            labelmode = vm["label_mode"].as<string>();
        }
    } catch (po::error &e) {
        cerr << "ERROR PARSING ARGUMENTS: " << e.what() << endl;
        return -1;
    }

    cout << "Usage: 'a' = previous pointcloud, 's' = exit, 'd' = next pointcloud" << endl;

    try {
        typedef vector<fs::path> vec;
        // store paths, so we can sort them later
        vec v;

        copy(fs::directory_iterator(basedir), fs::directory_iterator(), back_inserter(v));

        // sort, since directory iteration is not ordered on some file systems
        sort(v.begin(), v.end());

        //initialize poinclaa viewer
        pcl::visualization::PCLVisualizer viewer("Pointcloud");
        viewer.setBackgroundColor(0, 0, 0);
        viewer.registerKeyboardCallback(keyboardEventOccurred, (void *) &viewer);
        viewer.addCoordinateSystem(5);
        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr point_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>());
        pcl::console::setVerbosityLevel(pcl::console::L_ALWAYS);

        //iterate over all files in basedir directory
        vec::const_iterator it(v.begin());

        while (true) {
            //reload is initialized to go to next step in order to not get stuck in infinity loop
            //if first element ist not a pointcloud - to not start at second element, a bool is needed
            //to skip increasing iterator at the first time
            if(!first_loop)
                it += reload;
            else{
                first_loop = false;
            }
            if (it < v.begin()) it = v.end() - 1;
            if (it > v.end() - 1) it = v.begin();

            string filename = it.base()->string();

            //load poinclaa
            point_cloud->clear();

            // Try to load with RGB - if fails, points are black
            if (filename.substr(filename.size() -3) == "pcd")
                pcl::io::loadPCDFile<pcl::PointXYZRGBL>(filename, *point_cloud);
            else if (filename.substr(filename.size() -3) == "ply")
                pcl::io::loadPLYFile<pcl::PointXYZRGBL>(filename, *point_cloud);
            else{
                cerr << filename << " is not a valid pointcloud - continuing..." << endl;
                continue;
            }

            convertColorsByLabels(point_cloud, labelmode);

            //generate visualization
            viewer.removeAllPointClouds();
            viewer.setWindowName(it.base()->string());
            viewer.addPointCloud<pcl::PointXYZRGBL>(point_cloud, "cloud");

            //spin visualizer
            while (!keyPressed) viewer.spinOnce();

            //key was pressed
            if (reload == RELOAD_STATE_EXIT) break; //exit program

            keyPressed = false;
        }

    }
    catch (const fs::filesystem_error &e) {
        cout << e.what() << '\n';
    }

}