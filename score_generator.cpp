#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#define NUM_CLASSES 6  //Change for different dataset

using namespace std;
namespace fs = boost::filesystem;
namespace po = boost::program_options;

bool loadPointClouds(string &basedir,
                     string &filename,
                     pcl::PointCloud<pcl::PointXYZRGBL>::Ptr &inference_cloud,
                     pcl::PointCloud<pcl::PointXYZRGBL>::Ptr &GT_cloud){
    cout << "Loading: " + filename << endl;
    // Try to load with RGB - if fails, points are black
    string inference_path = basedir + "Inference/" + filename;
    string gt_path = basedir + "GT/" + filename;

    if (inference_path.substr(inference_path.size() -3) == "pcd")
        pcl::io::loadPCDFile<pcl::PointXYZRGBL>(inference_path, *inference_cloud);
    else if (inference_path.substr(inference_path.size() -3) == "ply")
        pcl::io::loadPLYFile<pcl::PointXYZRGBL>(inference_path, *inference_cloud);
    else{
        cerr << filename << " is not a valid pointcloud - continuing..." << endl;
        return -1;
    }
    if (gt_path.substr(gt_path.size() -3) == "pcd")
        pcl::io::loadPCDFile<pcl::PointXYZRGBL>(gt_path, *GT_cloud);
    else if (gt_path.substr(gt_path.size() -3) == "ply")
        pcl::io::loadPLYFile<pcl::PointXYZRGBL>(gt_path, *GT_cloud);
    else{
        cerr << filename << " is not a valid pointcloud - continuing..." << endl;
        return -1;
    }
    return true;
}

void writeOutputs(string basedir, string filename, int (&confusion_matrix)[6][6]){
    //Store output.
    std::fstream out((basedir + "Scores/" + filename.substr(0, filename.size()-4) + ".txt").c_str(), std::ios::out | std::ios::app);
    if (out.is_open())
    {
        out << "Confusion Matrix:" << endl;
        for (int i = 0; i < NUM_CLASSES; i++){
            for(int j = 0; j < NUM_CLASSES; j++){
                out << confusion_matrix[i][j] << " ";
            }
            out << endl;
        }
        out << endl << endl << "Intersection over Union:" << endl;

        float sum_iou = 0.0f;

        for (int i = 0; i < NUM_CLASSES; i++){
            int intersection = confusion_matrix[i][i];
            int union_ = 0;
            for (int j = 0; j < NUM_CLASSES; j++){
                union_ += confusion_matrix[i][j];
                union_ += confusion_matrix[j][i];
            }
            union_ -= intersection;
            float iou;
            if (union_ != 0) {
                iou = intersection / (float)union_;
            }
            else{
                //The class did not occur in GT but was also not predicted -> this is a 100% correct result.
                iou = 1;
            }
            out << iou << endl;
            sum_iou += iou;
        }

        out << endl << endl << "Mean Intersection over Union:" << endl;
        float miou = sum_iou / NUM_CLASSES;
        out << miou;

        out.close();
    }
}

int main(int argc, char **argv) {

    //Some variables with wide scope
    po::variables_map vm;
    string basedir;

    po::options_description desc("Allowed options");
    desc.add_options()
            ("help", "Print help message")
            ("input_directory", po::value<string>(), "Directory containing Pointclouds to display.");

    // Parse arguments
    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            cout << "This tool generates mIoU and confusion matrix from labeled data and inference. \n"
                 << "Usage: Provide an 'input_directory' which contains two folders: GT and Inference - each\n"
                 << "containing pointcloud files with labels. Note that for every pointcloud in Inference, a\n"
                 << "suitable GT pointcloud must be provided.\n";
            return 0;
        }
        //check if input is specified
        if (!vm.count("input_directory")) {
            cerr << "Please provide an input-directory." << endl;
            return -1;
        }
        //check if input is specified correctly
        basedir = vm["input_directory"].as<string>();
        size_t pos = basedir.find_last_of("/");
        if (pos != basedir.size()-1){
        basedir += "/";
        }
        if (!fs::is_directory(basedir)) {
            cerr << "Unable to find specified input-directory: " + basedir + " - Is it a file?" << endl;
            return -1;
        }if (!fs::is_directory(basedir + "Inference") || !fs::is_directory(basedir + "GT")) {
            cerr << "'Inference' and 'GT' folder have to be specified in input directory." << endl;
            return -1;
        }
    } catch (po::error &e) {
        cerr << "ERROR PARSING ARGUMENTS: " << e.what() << endl;
        return -1;
    }

    //Prepare output directory.
    fs::create_directory(basedir + "Scores");

    try {
        typedef vector<fs::path> vec;
        //Get list of files and sort it.
        vec v;
        copy(fs::directory_iterator(basedir + "Inference"), fs::directory_iterator(), back_inserter(v));
        sort(v.begin(), v.end());

        //Prepare containers.
        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr inference_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>());
        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr GT_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>());
        int confusion_matrix_overall[NUM_CLASSES][NUM_CLASSES];
        for (int i = 0; i < NUM_CLASSES;i++){
            for (int j = 0; j < NUM_CLASSES;j++){
                confusion_matrix_overall[j][i] = 0;
            }
        }

        //Iterate over all files in basedir directory.
        for (vec::const_iterator it(v.begin()); it != v.end() ; ++it) {

            string filename;

            size_t filename_pos = it->string().find_last_of('/');
            if (filename_pos != std::string::npos) {
                filename.assign(it->string().begin() + filename_pos + 1,
                                it->string().end());
            }

            //Load pointclouds.
            inference_cloud->clear();
            GT_cloud->clear();

            if(!loadPointClouds(basedir, filename, inference_cloud, GT_cloud)){
                continue;
            }

            int confusion_matrix[NUM_CLASSES][NUM_CLASSES];
            //Because not all compilers have the same zero-initializer.
            for (int i = 0; i < NUM_CLASSES;i++){
                for (int j = 0; j < NUM_CLASSES;j++){
                    confusion_matrix[i][j] = 0;
                }
            }

            pcl::PointCloud<pcl::PointXYZRGBL>::iterator gt_it = GT_cloud->begin();
            pcl::PointCloud<pcl::PointXYZRGBL>::iterator inference_it = inference_cloud->begin();
            //Reset confusion_matrix
            for (; inference_it != inference_cloud->end (); ++inference_it, ++gt_it) {
                //Count Prediction
                confusion_matrix[gt_it->label][inference_it->label]++;
                confusion_matrix_overall[gt_it->label][inference_it->label]++;

            }
                writeOutputs(basedir, filename, confusion_matrix);
            }
            writeOutputs(basedir, "Overall_Score.txt", confusion_matrix_overall);
        }

    catch (const fs::filesystem_error &e) {
        cout << e.what() << '\n';
    }
}