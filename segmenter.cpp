#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/segmentation/extract_clusters.h>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include <ctime>
#include <pcl/filters/passthrough.h>

#define NUM_LABELS 6  //inkl. 0-class
using namespace std;
namespace fs = boost::filesystem;
namespace po = boost::program_options;

bool slice(pcl::PointXYZRGBL &point){
    return ((point.x > -52 && point.x < -47) ||
            (point.x > -48 && point.x < -43) ||
            (point.x > -42 && point.x < -37) ||
            (point.x > -36 && point.x < -31) ||
            (point.x > -30 && point.x < -25) ||
            (point.x > -24 && point.x < -19) ||
            (point.x > -18 && point.x < -13) ||
            (point.x > -12 && point.x < -7)  ||
            (point.x > -6  && point.x < -1)  ||
            (point.x >  0  && point.x < 5)   ||
            (point.x >  6  && point.x < 11)  ||
            (point.x > 12  && point.x < 17)  ||
            (point.x > 18  && point.x < 23)  ||
            (point.x > 24  && point.x < 30)  ||
            (point.x > 31  && point.x < 36)  ||
            (point.x > 37  && point.x < 42)  ||
            (point.x > 43  && point.x < 48)  ||
            (point.x > 49  && point.x < 54)
        ) && (
            (point.y > -52 && point.y < -47) ||
            (point.y > -48 && point.y < -43) ||
            (point.y > -42 && point.y < -37) ||
            (point.y > -36 && point.y < -31) ||
            (point.y > -30 && point.y < -25) ||
            (point.y > -24 && point.y < -19) ||
            (point.y > -18 && point.y < -13) ||
            (point.y > -12 && point.y < -7)  ||
            (point.y > -6  && point.y < -1)  ||
            (point.y >  0  && point.y < 5)   ||
            (point.y >  6  && point.y < 11)  ||
            (point.y > 12  && point.y < 17)  ||
            (point.y > 18  && point.y < 23)  ||
            (point.y > 24  && point.y < 30)  ||
            (point.y > 31  && point.y < 36)  ||
            (point.y > 37  && point.y < 42)  ||
            (point.y > 43  && point.y < 48)  ||
            (point.y > 49  && point.y < 54));
}

int main(int argc, char **argv) {

    //Some variables with wide scope
    po::variables_map vm;
    fs::path basedir;

    po::options_description desc("Allowed options");
    desc.add_options()
            ("help", "Print help message")
            ("input_directory", po::value<string>(), "Directory containing Pointclouds to display.");

    // Parse arguments
    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            cout << "This tool uses region growing to generate clusters of same label (semantic instances).\n";
            return 0;
        }
        //check if input is specified
        if (!vm.count("input_directory")) {
            cerr << "Please provide an input-directory." << endl;
            return -1;
        }
        //check if input is specified correctly
        basedir = vm["input_directory"].as<string>();
        if (!fs::is_directory(basedir)) {
            cerr << "Unable to find specified input-directory: " + basedir.string() + " - Is it a file?" << endl;
            return -1;
        }
        if (is_empty(basedir)) {
            cerr << "The specified input directory is empty!" << endl;
            return -1;
        }
    } catch (po::error &e) {
        cerr << "ERROR PARSING ARGUMENTS: " << e.what() << endl;
        return -1;
    }

    try {
        typedef vector<fs::path> vec;
        // store paths, so we can sort them later
        vec v;

        copy(fs::directory_iterator(basedir), fs::directory_iterator(), back_inserter(v));

        //iterate over all files in basedir directory
        vec::const_iterator it(v.begin());

        for (; it != v.end(); ++it) {

            string filename = it.base()->string();
            cout << "Segmenting: " << filename << endl;
            clock_t start = std::clock(); //TODO:REMOVE

            //Load Cloud
            pcl::PointCloud<pcl::PointXYZRGBL>::Ptr point_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>());
            // Try to load with RGB - if fails, points are black
            if (filename.substr(filename.size() - 3) == "pcd")
                pcl::io::loadPCDFile<pcl::PointXYZRGBL>(filename, *point_cloud);
            else if (filename.substr(filename.size() - 3) == "ply")
                pcl::io::loadPLYFile<pcl::PointXYZRGBL>(filename, *point_cloud);
            else {
                cerr << filename << " is not a valid pointcloud - continuing..." << endl;
                continue;
            }
            cout << "Pointcloud loaded after " << (clock() - start) / (double) CLOCKS_PER_SEC <<  "s" << endl; //TODO:REMOVE
            start = clock(); //TODO:REMOVE

            //Intermediate cloud containers.
            pcl::PointCloud<pcl::PointXYZRGBL>::Ptr cloud_cluster(new pcl::PointCloud<pcl::PointXYZRGBL>);
            //KDTree for nearest neighbor search.
            pcl::search::KdTree<pcl::PointXYZRGBL>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBL>);
            tree->setInputCloud(point_cloud);

            for (int label = 0; label < NUM_LABELS; label++) {
                if (label == 0 || label == 5)
                    continue;   //Skip scatter class and cars.

                int counter = 0;
                pcl::PointCloud<pcl::PointXYZRGBL>::Ptr query_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>());
                clock_t start4 = clock(); //TODO:REMOVE
                for (pcl::PointCloud<pcl::PointXYZRGBL>::iterator points_it = point_cloud->begin();
                     points_it != point_cloud->end(); ++points_it, ++counter) {

                    //Conditions for adding to query_cloud vary among labels. Labels which tend to contain
                    //very large clusters get sliced.
                    if (label == 1 && points_it->label == label
                            && counter%10 ==0
                            && slice(*points_it)) {
                        query_cloud->push_back(*points_it);
                        continue;
                    }
                    if (label == 2 && points_it->label == label
                            && counter%10 == 0){
                        query_cloud->push_back(*points_it);
                        continue;
                    }
                    if (label == 3 && points_it->label == label
                        && counter%10 ==0
                            && slice(*points_it)){
                        query_cloud->push_back(*points_it);
                        continue;
                    }
                    if (label == 4 && points_it->label == label
                            && counter%10 ==0){
                        query_cloud->push_back(*points_it);
                        continue;
                    }
                }
                cout << "Downsampling took " << (clock() - start4) / (double) CLOCKS_PER_SEC << "s" << endl; //TODO:REMOVE

                pcl::EuclideanClusterExtraction<pcl::PointXYZRGBL> lec;

                clock_t start2 = clock(); //TODO:REMOVE

                //Euclidean Cluster Extraction is parametrized differently for every label.

                if (label == 1){ //Ground plane
                    lec.setMinClusterSize(50);
                    lec.setMaxClusterSize(10000);
                    lec.setClusterTolerance(0.25);
                }
                if (label == 2){ //Vegetation
                    lec.setMinClusterSize(125);
                    lec.setMaxClusterSize(1000);
                    lec.setClusterTolerance(0.3);
                }
                if (label == 3){ //Building
                    lec.setMinClusterSize(125);
                    lec.setMaxClusterSize(10000);
                    lec.setClusterTolerance(0.25);
                }
                if (label == 4){ //Man-Made Structure
                    lec.setMinClusterSize(50);
                    lec.setMaxClusterSize(1000);
                    lec.setClusterTolerance(0.25);
                }

                std::vector<pcl::PointIndices> cluster_indices;
                lec.setInputCloud(query_cloud);
                lec.setSearchMethod(tree);
                lec.extract(cluster_indices);

                cout << "Found " << cluster_indices.size() << " clusters for label " << label  << " in " << (clock() - start2)/ (double) CLOCKS_PER_SEC << "s" << endl; //TODO:REMOVE TIME STUFF

                cout << "Calculating COGs" << endl;
                clock_t start3 = clock(); //TODO:REMOVE
                for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it) {
                    float sum_x = 0.0f;
                    float sum_y = 0.0f;
                    float sum_z = 0.0f;
                    int points_number = 0;

                    for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit){
                        sum_x += query_cloud->points[*pit].x;
                        sum_y += query_cloud->points[*pit].y;
                        sum_z += query_cloud->points[*pit].z;
                        points_number++;
                    }
                    pcl::PointXYZRGBL point;
                    point.label = label;

                    point.x = sum_x / points_number;
                    point.y = sum_y / points_number;
                    point.z = sum_z / points_number;
                    point.r = 0;
                    point.g = 0;
                    point.b = 0;

                    cloud_cluster->points.push_back(point);
                }
                cout << "COG calculation took: " << (clock() - start3) / (double) CLOCKS_PER_SEC << "s" << endl; //TODO:REMOVE
            }
            cout << "Done after: " << (clock() - start) / (double) CLOCKS_PER_SEC << "s" << endl; //TODO:REMOVE

            //TODO:REMOVE ALL BELOW ------------------------------------------------------------------------------------
            //initialize poinclaa viewer
            pcl::visualization::PCLVisualizer viewer("Pointcloud");
            viewer.addPointCloud<pcl::PointXYZRGBL>(point_cloud, "cloud");

            viewer.setBackgroundColor(0, 0, 0);
            viewer.addCoordinateSystem(5);
            pcl::console::setVerbosityLevel(pcl::console::L_ALWAYS);

            for (pcl::PointCloud<pcl::PointXYZRGBL>::iterator points_it = cloud_cluster->begin();
                 points_it != cloud_cluster->end(); ++points_it) {
                pcl::ModelCoefficients sphere_coeff;
                sphere_coeff.values.resize(4);    // We need 4 values: x, y, z, radius.
                sphere_coeff.values[0] = points_it->x;
                sphere_coeff.values[1] = points_it->y;
                sphere_coeff.values[2] = points_it->z;
                sphere_coeff.values[3] = 0.4;
                std::ostringstream ss;
                ss << points_it->x;
                std::string s(ss.str());
                viewer.addSphere(sphere_coeff, s);
                if (points_it->label == 0)  //White = clutter
                    viewer.setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1, 1, 1, s, 0);
                if (points_it->label == 1)
                    viewer.setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0, 1, 1, s, 0);
                if (points_it->label == 2)
                    viewer.setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0, 1, 0, s, 0);
                if (points_it->label == 3)
                    viewer.setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1, 0, 0, s, 0);
                if (points_it->label == 4)
                    viewer.setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1, 0, 1, s, 0);
                if (points_it->label == 5)
                    viewer.setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0, 1, s, 0);

                viewer.setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 1, s, 0);
            }
            //generate visualization
            viewer.setWindowName(it.base()->string());
            viewer.spin();
            //TODO:REMOVE ALL ABOVE ------------------------------------------------------------------------------------
        }
    }
    catch (const fs::filesystem_error &e) {
        cout << e.what() << '\n';
    }

}